package fr.esimed.tp

fun levenshteinDistance(chaine1:String, chaine2:String) : Int {
    val chaine1Longueur = chaine1.length
    val chaine2Longueur = chaine2.length

    var d = Array(chaine1Longueur+1, {IntArray(chaine2Longueur+1)})
    var coutSubstitution:Int

    for (i in 0..chaine1Longueur) {
        d[i][0] =  i
    }

    for (j in 0..chaine2Longueur) {
        d[0][j] = j
    }

    for (i in 1..chaine1Longueur) {
        for (j in 1..chaine2Longueur) {
            if (chaine1[i-1] == chaine2[j-1]) {
                coutSubstitution = 0
            }
            else  {
                coutSubstitution = 1
            }
            d[i][j] = minOf(
                // effacement
                d[i-1][j]+1,
                // insertion
                d[i][j-1]+1,
                // substitution
                d[i-1][j-1]+coutSubstitution
            )
        }
    }
    return d[chaine1Longueur][chaine2Longueur]
}

fun percentMatch(chaine1:String, chaine2:String) : Int {
    val chaine1Longueur = chaine1.length
    val chaine2Longueur = chaine2.length
    val distanceDeLevenshtein = levenshteinDistance(chaine1,chaine2)
    val result = 100 - (distanceDeLevenshtein * 100).toFloat() / (chaine1Longueur + chaine2Longueur).toFloat()
    return result.toInt()
}