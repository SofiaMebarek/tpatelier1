package fr.esimed.tp.test

import fr.esimed.tp.levenshteinDistance
import fr.esimed.tp.percentMatch
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.function.Executable

class TextToolsTests {
    @Test
    fun levenshteinDistanceTest() {
        Assertions.assertAll(
            Executable { Assertions.assertEquals(6, levenshteinDistance("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(1, levenshteinDistance("Julie", "Julien")) }
        )
    }

    @Test
    fun percentMatchTest() {
        Assertions.assertAll(
            Executable { Assertions.assertEquals(50, percentMatch("Arnaud", "Julien")) },
            Executable { Assertions.assertEquals(90, percentMatch("Julie", "Julien")) }
        )
    }
}